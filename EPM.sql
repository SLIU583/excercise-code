/* Build primary job flag */ 
with 
EMPL_CLASS_RANK as (
    select 'LI' as emplClass, 1 as emplClassRank from dual
    union
    select 'FA' as emplClass, 2 as emplClassRank from dual
    union
    select 'AS' as emplClass, 3 as emplClassRank from dual
    union
    select 'CP' as emplClass, 4 as emplClassRank from dual
    union
    select 'CJ' as emplClass, 5 as emplClassRank from dual
    union
    select 'CL' as emplClass, 6 as emplClassRank from dual
    union
    select 'OT1' as emplClass, 7 as emplClassRank from dual
    union
    select 'ET' as emplClass, 8 as emplClassRank from dual
    union
    select 'SA' as emplClass, 9 as emplClassRank from dual
    union
    select 'SH' as emplClass, 10 as emplClassRank from dual
    union
    select 'OT2' as emplClass, 11 as emplClassRank from dual
    union
    select 'OT3' as emplClass, 12 as emplClassRank from dual
    union
    select 'OT4' as emplClass, 13 as emplClassRank from dual
),
EMPL_PAY_RANK as (
    select 'A' as payBasis, 1 as emplPayRank from dual
    union
    select 'C' as payBasis, 2 as emplPayRank from dual
    union
    select 'H' as payBasis, 3 as emplPayRank from dual
    union
    select 'L' as payBasis, 4 as emplPayRank from dual
    union
    select 'S' as payBasis, 5 as emplPayRank from dual
    union
    select 'V' as payBasis, 6 as emplPayRank from dual
    union
    select 'N' as payBasis, 7 as emplPayRank from dual
),
CTE1 as (
    select
     EMPLID,
     EMPL_RCD,
     FTE, 
     DEPTID,
     emplClass,
     payBasis,
     UW_PAY_BASIS,
     EMPL_CLASS,
     RANK() over (partition by EMPLID, EMPL_RCD order by EFFDT desc, EFFSEQ desc, COMP_EFFSEQ desc) as actionRank
     from PS_UW_HR_ALLJOB_VW 
     left join EMPL_CLASS_RANK on EMPL_CLASS_RANK.emplClass = PS_UW_HR_ALLJOB_VW.EMPL_CLASS
     left join EMPL_PAY_RANK on EMPL_PAY_RANK.payBasis = PS_UW_HR_ALLJOB_VW.UW_PAY_BASIS
     where
     UW_JOB_START_DATE <= CURRENT_DATE and (UW_JOB_END_DT is NULL or UW_JOB_END_DT >= CURRENT_DATE)
     and 
     BUSINESS_UNIT = 'UWMSN'
     and 
     EMPL_CLASS != 'SH'
),
CTE2 as (
    select CTE1.*,  RANK() over (partition by EMPLID order by FTE desc, emplClass asc, payBasis asc, EMPL_RCD) as classRank
    from CTE1 
    where actionRank = 1
)

select 
     substr(deptid, 1, 3) as division, 
     count(EMPLID) as numOfEmpl, 
     count(*) as numOfJobs
from CTE2
where classRank = 1  
group by substr(deptid, 1, 3)
order by substr(deptid, 1, 3)

select 
     EMPL_CLASS as emplClass, 
     count(EMPLID) as numOfEmpl, 
     count(*) as numOfJobs
from CTE2
where classRank = 1  
group by EMPL_CLASS
order by EMPL_CLASS