# Excercise Code

Excercise Code in EPM

## BN - Rebuild Primary Job Flag 

### General Description: 

Primary Job Flags drive benefit eligibility and when benefit deductions are 
taken. When the Flag is set to a job that the employee is not paid on, 
deductions will not taken from that payroll. The system will automatically assess 
each JOB row action to determine if the Benefit Flag Assignment is appropriate
and will set the Primary Job. 

### Process Considerations:
* Primary Job Flags will be set based on the following criteria:
1. If there is only one active non-Student Help (SH) job, this job will be 
flagged as the Benefits Primary 
2. If multiple active non-SH jobs exist, the job with the maximum FTE is set
as the Benefits Primary 
3. If multiple active non-SH jobs exist with the same FTE, the Benefits Primary
Job will be based on the following order of priority in Employee Class (
Empl_class): LI, FA, AS, CP, CJ, CL, OT1, ET, SA, SH, OT2, OT3, OT4,... etc. 
4. If multiple active non-SH jobs exist, with the same FTE and Empl_Class, the 
Primary Flag will be based on the following order of priority in Pay Basis on 
JOB: A(Annual), C(9 month), H(hourly), L(Lump), S(Summer Session), V(Summer 
Service), N(Non).

Lessons: 
1. Use CTE to make the code readable and more compact. It also makes it 
easier to test. 
2. When testing SQL, use *count* and *distinct* 
3. could *order by* multiple columns 